<?php



class Peminjaman extends Eloquent {

	
	protected $table = 'peminjaman';

	public function buku(){
		return	$this->hasOne('Buku' , 'id'  , 'id_buku');
	}

	public function anggota(){
		return	$this->hasOne('Anggota' , 'id'  , 'id_member');
	}

}

