<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('before' => 'auth'), function()
{

	Route::get('/',array('uses'=>'dashboardController@index'));
	Route::get('buku/',array('uses'=>'bukuController@index'));
	Route::get('printBarcode/',array('uses'=>'bukuController@printBarcode'));
	Route::get('kategori/',array('uses'=>'bukuController@kategori'));
	Route::post('cekKode/',array('uses'=>'bukuController@cekKode'));
	
	Route::post('tambahBuku/',array('uses'=>'bukuController@tambahBuku'));
	Route::post('editBuku/',array('uses'=>'bukuController@editBuku'));
	Route::post('editBuku/{id}',array('uses'=>'bukuController@editBukuProses'));
	Route::get('delBuku/{id}',array('uses'=>'bukuController@delBuku'));
	Route::post('tambahKat',array('uses'=>'bukuController@tambahKat'));
	Route::post('editKat',array('uses'=>'bukuController@editKat'));
	Route::post('editKat/{id}',array('uses'=>'bukuController@editKatProses'));
	Route::get('delKat/{id}',array('uses'=>'bukuController@delKat'));
	

	Route::get('member/',array('uses'=>'aktifitasController@member'));
	Route::post('cekKodeMember/',array('uses'=>'aktifitasController@cekKode'));
	Route::post('tambahMember/',array('uses'=>'aktifitasController@tambahMember'));
	Route::post('editMember',array('uses'=>'aktifitasController@editMember'));
	Route::post('editMember/{id}',array('uses'=>'aktifitasController@editMemberProses'));
	Route::get('delMember/{id}',array('uses'=>'aktifitasController@delMember'));
	

	Route::get('peminjaman/',array('uses'=>'aktifitasController@peminjaman'));
	Route::get('cekBuku/',array('uses'=>'aktifitasController@cekBuku'));
	Route::get('cekMember/',array('uses'=>'aktifitasController@cekMember'));
	Route::post('tambahPeminjaman/',array('uses'=>'aktifitasController@tambahPeminjaman'));

	Route::post('editPeminjaman',array('uses'=>'aktifitasController@editPeminjaman'));
	Route::post('editPeminjaman/{id}',array('uses'=>'aktifitasController@editPeminjamanProses'));
	Route::get('delPeminjaman/{id}',array('uses'=>'aktifitasController@delPeminjaman'));
	Route::get('pengembalian/{id}',array('uses'=>'aktifitasController@pengembalian'));
	Route::get('kembali',array('uses'=>'aktifitasController@kembali'));
	Route::get('perpustakaan',array('uses'=>'aktifitasController@perpustakaan'));
	Route::post('updatePerpus',array('uses'=>'aktifitasController@updatePerpus'));

	Route::get('admin',array('uses'=>'aktifitasController@admin'));
	Route::get('tambahAdmin',array('uses'=>'aktifitasController@tambahAdmin'));
	Route::post('tambahAdmin',array('uses'=>'aktifitasController@tambahAdminProses'));
	Route::post('updateAdmin',array('uses'=>'aktifitasController@updateAdmin'));
	Route::get('visitor',array('uses'=>'aktifitasController@visitor'));
	Route::post('tambahVisitor',array('uses'=>'aktifitasController@tambahVisitor'));


	Route::get('grafik',array('uses'=>'aktifitasController@grafik'));

	Route::get('pdfBuku',array('uses'=>'pdfController@index'));



});


Route::get('login', array('uses' => 'HomeController@showLogin'));	
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('logout', array('uses' => 'HomeController@doLogout'));
