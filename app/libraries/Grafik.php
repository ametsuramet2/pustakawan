<?php 

class Grafik {

	public static function Peminjaman($time){
		switch ($time) {
			case 'today':
				$daterange = Grafik::dateRange(date('Y-m-d'),date('Y-m-d'));
			break;

			case 'yesterday':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('-1 days')),date('Y-m-d'  , strtotime('-1 days')));
			break;
			case 'lastweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('last week')),date('Y-m-d'  , strtotime('last sunday')));
			break;
			case 'thisweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('this  week')),date('Y-m-d'  , strtotime('next sunday')));
			break;
			case 'thismonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this month')),date('Y-m-d'  , strtotime('last day of this month')));
			break;
			
			case 'lastmonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of last month')),date('Y-m-d'  , strtotime('last day of last month')));
			break;

			case 'thisyear':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this year')),date('Y') . '-12-31');
			break;
			
			default:
				# code...
				break;
		}
		
		foreach($daterange as $index=>$dr){
			$peminjaman = Peminjaman::where('pinjam','>=',$dr.' 00:00:00')
			->where('pinjam','<=',$dr.' 23:59:59')->get();
			$jumlah = 0;
			foreach ($peminjaman as  $p) {
				$jumlah+=$p->jumlah;
				
			}
			$data[] = array('tgl'=>$dr,'jumlah'=>$jumlah);
		}
		return $data;
	}
	public static function Pengembalian($time){
		switch ($time) {
			case 'today':
				$daterange = Grafik::dateRange(date('Y-m-d'),date('Y-m-d'));
			break;

			case 'yesterday':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('-1 days')),date('Y-m-d'  , strtotime('-1 days')));
			break;
			case 'lastweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('last week')),date('Y-m-d'  , strtotime('last sunday')));
			break;
			case 'thisweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('this  week')),date('Y-m-d'  , strtotime('next sunday')));
			break;
			case 'thismonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this month')),date('Y-m-d'  , strtotime('last day of this month')));
			break;
			
			case 'lastmonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of last month')),date('Y-m-d'  , strtotime('last day of last month')));
			break;

			case 'thisyear':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this year')),date('Y') . '-12-31');
			break;
			
			default:
				# code...
				break;
		}
		foreach($daterange as $index=>$dr){
			$peminjaman = Peminjaman::where('status',1)->where('updated_at','>=',$dr.' 00:00:00')
			->where('updated_at','<=',$dr.' 23:59:59')->get();
			$jumlah = 0;
			foreach ($peminjaman as  $p) {
				$jumlah+=$p->jumlah;
				
			}
			$data[] = array('tgl'=>$dr,'jumlah'=>$jumlah);
		}
		return $data;
	}

	public static function Pengunjung($time){
		switch ($time) {
			case 'today':
				$daterange = Grafik::dateRange(date('Y-m-d'),date('Y-m-d'));
			break;

			case 'yesterday':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('-1 days')),date('Y-m-d'  , strtotime('-1 days')));
			break;
			case 'lastweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('last week')),date('Y-m-d'  , strtotime('last sunday')));
			break;
			case 'thisweek':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('this  week')),date('Y-m-d'  , strtotime('next sunday')));
			break;
			case 'thismonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this month')),date('Y-m-d'  , strtotime('last day of this month')));
			break;
			
			case 'lastmonth':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of last month')),date('Y-m-d'  , strtotime('last day of last month')));
			break;

			case 'thisyear':
				$daterange = Grafik::dateRange(date('Y-m-d'  , strtotime('first day of this year')),date('Y') . '-12-31');
			break;
			
			default:
				# code...
				break;
		}
		foreach($daterange as $index=>$dr){
			$count = Pengunjung::where('created_at','>=',$dr.' 00:00:00')
			->where('created_at','<=',$dr.' 23:59:59')->count();
			
			$data[] = array('tgl'=>$dr,'jumlah'=>$count);
		}
		return $data;
	}


	public static function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

		$dates = array();
		$current = strtotime( $first );
		$last = strtotime( $last );

		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}

		return $dates;
	}
}