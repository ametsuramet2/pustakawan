<?php

class AdminSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$admin = array(
			'nama' => 'Amet',
			'username' => 'ametsuramet', 'password' => Hash::make('gagaro666'),
			'created_at' => new DateTime, 'updated_at' => new DateTime
		);

		DB::table('users')->insert($admin);
	}

}