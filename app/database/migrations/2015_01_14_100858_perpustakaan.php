<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Perpustakaan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('perpustakaan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('logo', 200)->nullable();
			$table->string('foto', 200)->nullable();
			$table->string('nama', 50);
			$table->string('alamat',255)->nullable();
			$table->string('telp',50)->nullable();
			$table->string('email',50)->nullable();
			$table->string('website',50)->nullable();
			$table->longtext('deskripsi')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('perpustakaan');
	}

}
