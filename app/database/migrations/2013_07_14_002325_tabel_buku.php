<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelBuku extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('buku', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 30)->nullable();
			$table->string('cover', 200)->nullable();
			$table->string('judul', 100);
			$table->string('penulis', 100)->nullable();
			$table->string('penerbit', 100)->nullable();
			$table->integer('tahun')->nullable();
			$table->integer('jumlah');
			$table->integer('kategori');
			$table->integer('status');
			$table->string('pemilik',255)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('buku');
	}

}
