<?php

class bukuController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$buku = Buku::with('katbuku')->get();
		$kat = Kat::all();

		$this->layout = View::make('master');
		$this->layout->content = View::make('buku')->with('data',$buku)->with('kategori',$kat);
	}

	public function printBarcode()
	{
		$buku = Buku::with('katbuku');
		if(isset($_GET['id'])){
			if(strpos($_GET['id'], '-')){
				$ids = explode('-',$_GET['id']);
				// print_r($ids);
				$buku = $buku->where('id','>=',$ids[0])->where('id','<=',$ids[1]);
				// die();
			}else{

			$id = explode(',',$_GET['id']);
			$buku = $buku->whereIn('id',$id);
			}
		}

		$buku = $buku->get();
		   $queries = DB::getQueryLog();
		   // print_r($queries);
		$kat = Kat::all();

		$this->layout = View::make('master');
		$this->layout->content = View::make('printBarcode')->with('data',$buku)->with('kategori',$kat);
	}

	public function editBuku()
	{
		$buku = Buku::find(Input::get('id'));
		return Response::json($buku);
	}


	public function editBukuProses($id)
	{
		//print_r(Input::all());
		$buku = Buku::find($id);
		$buku->code = Input::get('kode');
		$buku->judul = Input::get('judul');
		$buku->kategori = Input::get('kategori');
		$buku->penulis = Input::get('penulis');
		$buku->penerbit = Input::get('penerbit');
		$buku->tahun = Input::get('tahun');
		$buku->jumlah = Input::get('jumlah');
		$buku->status = Input::get('status');
		$buku->pemilik = Input::get('pemilik');

		if(Input::file('cover')){
		$file = Input::file('cover');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('cover')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$buku->cover = $filename;
		}


		
		$buku->save();

		return Redirect::back()->with('success', ' Edit Buku : '.Input::get('judul').' sukses');
		
	}



	public function editKatProses($id)
	{
		//print_r(Input::all());
		$kat = Kat::find($id);
		$kat->kategori = Input::get('kategori');
		
		$kat->save();
		return Redirect::back()->with('success', ' Edit Buku : '.Input::get('kategori').' sukses');
		
	}

	public function cekKode()
	{
		$cek = Buku::where('code','like','%'.Input::get('kode').'%')->count();
		echo $cek;
	}

	public function delBuku($id)
	{
		$buku = Buku::find($id);
		$judul = $buku->judul;
		Buku::destroy($id);
		return Redirect::back()->with('success', ' Delete Buku : '.$judul.' sukses');
	}


	
	public function tambahBuku()
	{ 
		//print_r(Input::all());
		$filename = null;
		if(Input::file('cover')){
		$file = Input::file('cover');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('cover')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		}

		$buku = new Buku;
		$buku->code = Input::get('kode');
		$buku->judul = Input::get('judul');
		$buku->kategori = Input::get('kategori');
		$buku->penulis = Input::get('penulis');
		$buku->penerbit = Input::get('penerbit');
		$buku->tahun = Input::get('tahun');
		$buku->jumlah = Input::get('jumlah');
		$buku->status = Input::get('status');
		$buku->pemilik = Input::get('pemilik');
		$buku->cover = $filename;
		$buku->save();

		return Redirect::back()->with('success', ' Tambah Buku : '.Input::get('judul').' sukses');
	}


	


	public function kategori()
	{
		$kat = Kat::all();
		$this->layout = View::make('master');
		$this->layout->content = View::make('kategori')->with('data',$kat);
	}


	public function tambahKat()
	{
		$kat = new Kat;
		$kat->kategori = Input::get('kategori');
		$kat->save();
		return Redirect::back()->with('success', ' Tambah kategori : '.Input::get('kategori').' sukses');
	}

	public function editKat()
	{
		$kat = Kat::find(Input::get('id'));
		return Response::json($kat);
	}

	public function delKat($id)
	{
		$kat = Kat::find($id);
		$judul = $kat->kategori;
		Kat::destroy($id);
		return Redirect::back()->with('success', ' Delete Kategori : '.$judul.' sukses');
	}

}
