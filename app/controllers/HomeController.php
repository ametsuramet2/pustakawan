<?php
use Illuminate\Support\MessageBag;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLogin()
	{
		

		if(Auth::guest()){
			
			$this->layout = View::make('master');
			$this->layout->content = View::make('login');
		}else{
			
			return Redirect::to('/');
			
		}
	}

	public function doLogin()
	{
			$remember = false;
			if(Input::get('remember_me')=="on"){
					$remember = true;
					}
			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password'),
				
			);

			$userdata2 = array(
				'username' 	=> Input::get('email'),
				'password' 	=> Input::get('password'),				
				);


			if(Auth::attempt($userdata,$remember)) {				
				
					return Redirect::to('/');
				
			}elseif(Auth::attempt($userdata2,$remember)) {	 	
				
					return Redirect::to('/');
				
			}else{
				$errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
				return Redirect::to('login')->withErrors($errors)->withInput(Input::except('password'));
		 	}

	}

	public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		return Redirect::to('login'); // redirect the user to the login screen
	}

}
