<?php

class aktifitasController extends \BaseController {


	public function member()
	{
		$member = Anggota::all();

		$this->layout = View::make('master');
		$this->layout->content = View::make('member')->with('data',$member);
	}

	

	public function cekKode()
	{
		$cek = Anggota::where('kode','like','%'.Input::get('kode').'%')->count();
		echo $cek;
	}

	public function tambahMember()
	{ 
		//print_r(Input::all());
		$filename = null;
		if(Input::file('cover')){
		$file = Input::file('cover');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('cover')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		}

		$member = new Anggota;
		$member->kode = Input::get('kode_member');
		$member->nama = Input::get('nama');
		$member->telp = Input::get('telp');
		$member->alamat = Input::get('alamat');	
		$member->foto = $filename;
		$member->save();

		return Redirect::back()->with('success', ' Tambah Member : '.Input::get('nama').' sukses');
	}

	

	public function editMember()
	{
		$buku = Anggota::find(Input::get('id'));
		return Response::json($buku);
	}


	public function editMemberProses($id)
	{ 
		//print_r(Input::all());
		$filename = null;
		
		$member = Anggota::find($id);
		$member->kode = Input::get('kode_member');
		$member->nama = Input::get('nama');
		$member->telp = Input::get('telp');
		$member->alamat = Input::get('alamat');	
		if(Input::file('cover')){
		$file = Input::file('cover');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('cover')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$member->foto = $filename;
		}
		
		$member->save();

		return Redirect::back()->with('success', ' Edit Member : '.Input::get('nama').' sukses');
	}

	public function delMember($id)
	{
		$member = Anggota::find($id);
		$name = $member->nama;
		Anggota::destroy($id);
		return Redirect::back()->with('success', ' Delete Member : '.$name.' sukses');
	}

	
	public function peminjaman()
	{
		$pinjam = Peminjaman::with('anggota','buku')->get();

		$this->layout = View::make('master');
		$this->layout->content = View::make('peminjaman')->with('data',$pinjam);
	}


	public function cekBuku()
	{
		$data = array();
		$q=$_GET['term'];		
		$item = Buku::where('judul','like','%'.$q.'%')
		->orWhere('penulis','like','%'.$q.'%')
		->orWhere('penerbit','like','%'.$q.'%')->get();
	
		foreach($item as $items){

				$label = $items->judul;
				$data[]  = array(
				'label'=>''.$label,
				'value'=>$items->id,
				);
		
			// /echo $data->id;
		}	

		echo json_encode($data);
	}

	public function cekMember()
	{
		$data = array();
		$q=$_GET['term'];		
		$item = Anggota::where('nama','like','%'.$q.'%')
		->orWhere('kode','like','%'.$q.'%')
		->orWhere('alamat','like','%'.$q.'%')->get();
	
		foreach($item as $items){

				$label = $items->nama;
				$data[]  = array(
				'label'=>''.$label,
				'value'=>$items->id,
				);
		
			// /echo $data->id;
		}	

		echo json_encode($data);
	}


	public function tambahPeminjaman()
	{
		// print_r(Input::all());
		$pinjam = new Peminjaman;
		$pinjam->id_buku = Input::get('id_buku');
		$pinjam->id_member = Input::get('id_member');
		$pinjam->jumlah = Input::get('jumlah');
		$pinjam->pinjam = Input::get('pinjam');
		$pinjam->kembali = Input::get('kembali');
		$pinjam->save();
		return Redirect::back()->with('success', ' Tambah Peminjaman : '.Input::get('judul_buku').' sukses');
	}

	public function editPeminjamanProses($id)
	{
		// print_r(Input::all());
		$pinjam = Peminjaman::find($id);
		$pinjam->id_buku = Input::get('id_buku');
		$pinjam->id_member = Input::get('id_member');
		$pinjam->jumlah = Input::get('jumlah');
		$pinjam->pinjam = Input::get('pinjam');
		$pinjam->kembali = Input::get('kembali');
		$pinjam->save();
		return Redirect::back()->with('success', ' Edit Peminjaman : '.Input::get('judul_buku').' sukses');
	}

	public function pengembalian($id)
	{
		// print_r(Input::all());
		$pinjam = Peminjaman::find($id);
		$pinjam->status = 1;
		
		
		$pinjam->save();
		return Redirect::back()->with('success', ' Peminjaman : '.$pinjam->buku->judul.' sukses di kembalikan');
	}

	public function pengembalianRestore($id)
	{
		// print_r(Input::all());
		$pinjam = Peminjaman::find($id);
		$pinjam->status = 0;
		
		
		$pinjam->save();
		return Redirect::back()->with('success', ' Peminjaman : '.$pinjam->buku->judul.' di restore');
	}


	public function editPeminjaman()
	{
		$pinjam = Peminjaman::with('anggota','buku')->find(Input::get('id'));
		return Response::json($pinjam);

	}

	public function delPeminjaman($id)
	{
		$pinjam = Peminjaman::find($id);
		$name = $pinjam->buku->judul;
		Peminjaman::destroy($id);
		return Redirect::back()->with('success', ' Delete Peminjaman : '.$name.' sukses');
	}

	public function kembali()
	{
		$pinjam = Peminjaman::where('status',1)->with('anggota','buku')->get();

		$this->layout = View::make('master');
		$this->layout->content = View::make('pengembalian')->with('data',$pinjam);
	}

	public function grafik()
	{
		$grafik = array();
		$this->layout = View::make('master');
		$this->layout->content = View::make('grafik')->with('data',$grafik);
	}

	public function perpustakaan()
	{
		$perpustakaan = Perpus::first();
		$this->layout = View::make('master');
		$this->layout->content = View::make('perpustakaan')->with('data',$perpustakaan);
	}



	public function updatePerpus()
	{

		$perpustakaan = Perpus::find(Input::get('id'));
		$perpustakaan->nama = Input::get('nama');
		$perpustakaan->alamat = Input::get('alamat');
		$perpustakaan->telp = Input::get('telp');
		$perpustakaan->email = Input::get('email');
		$perpustakaan->website = Input::get('website');
		$perpustakaan->deskripsi = Input::get('deskripsi');
		if(Input::file('cover')){
		$file = Input::file('cover');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('cover')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$perpustakaan->logo = $filename;
		}
		if(Input::file('foto')){
		$file = Input::file('foto');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('foto')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$perpustakaan->foto = $filename;
		}

		// print_r($perpustakaan);
		$perpustakaan->save();

		return Redirect::back()->with('success', ' Update Date Perpustakaan Sukses');
	
		
	}

	public function admin(){
		$admin = Auth::user();
		$this->layout = View::make('master');
		$this->layout->content = View::make('admin')->with('data',$admin);
	}
	public function tambahAdmin(){
		
		$this->layout = View::make('master');
		$this->layout->content = View::make('tambahAdmin');
	}

	public function tambahAdminProses(){

		$admin = new User;
		$admin->nama = Input::get('nama');
		$admin->username = Input::get('username');
		$admin->email = Input::get('email');
		
		$admin->password = Hash::make(Input::get('password'));
		
		if(Input::file('foto')){
		$file = Input::file('foto');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('foto')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$admin->foto = $filename;
		}

		// print_r($perpustakaan);
		$admin->save();

		return Redirect::back()->with('success', ' Tambah Admin Perpustakaan Sukses');
	}
	public function updateAdmin(){

		$admin = Auth::user();
		$admin->nama = Input::get('nama');
		$admin->username = Input::get('username');
		$admin->email = Input::get('email');
		
		if(Input::get('password')){
		$admin->password = Hash::make(Input::get('password'));
		}
		if(Input::file('foto')){
		$file = Input::file('foto');	
		$destinationPath =public_path().'/images/'; //Path in your public folder...
		$filename = $file->getClientOriginalName();
		Input::file('foto')->move($destinationPath, $filename);
		$filePath =  URL::to('/images').'/'.$filename;
		$admin->foto = $filename;
		}

		// print_r($perpustakaan);
		$admin->save();

		return Redirect::back()->with('success', ' Update Admin Perpustakaan Sukses');
	}

	public function visitor(){
		$visit = Pengunjung::with('anggota')->get();
		$this->layout = View::make('master');
		$this->layout->content = View::make('visitor')->with('data',$visit);
	}
	public function tambahVisitor(){
		$visit = new Pengunjung;
		if(Input::get('id_visitor'))
		$visit->id_visitor = Input::get('id_visitor');
		$visit->nama = Input::get('nama');
		$visit->save();
		return Redirect::back()->with('success', 'Menambah visitor : '. Input::get('nama') .'  Sukses');
	}


}
