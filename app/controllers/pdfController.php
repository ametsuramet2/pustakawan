<?php

class pdfController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Buku::with('katbuku')->orderBy('id','desc');
			$skip = 0;

		if(isset($_GET['take'])){
			$data = $data->take($_GET['take']);
		}
		if(isset($_GET['skip'])){
			$data = $data->skip($_GET['skip']);
			$skip = $_GET['skip'];
		}
		$data = $data->get();
		$html =  View::make('pdf.buku')->with('data',$data)->with('skip',$skip); 
		$pdf = App::make('dompdf');
		$pdf->loadHTML($html);

		return $pdf->setPaper('a4')->setOrientation('landscape')->setWarnings(false)->stream('buku.pdf');
	}
 

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
