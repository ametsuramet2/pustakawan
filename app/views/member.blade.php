@section('title')
Daftar Buku | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">
<div class="row">
    <div class="col-md-3 pull-right">
    <button type="button" class="btn btn-danger btn-tambahMember btn-block" data-toggle="modal" data-target="#myModal">
   <span class="glyphicon glyphicon-plus"></span>   Tambah Member
    </button>


    </div>
</div>
<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama</th>
              <th>Telp</th>
              <th>Alamat</th>
             
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$m)
         
            <tr data-id="{{$m->id}}">
              <td>{{$i+1}}</td>
              <td>{{$m->kode}}</td>
              <td>{{$m->nama}}</td>
              <td>{{$m->telp}}</td>
             
              <td>{{$m->alamat}}</td>
              
              <td>
                  <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-danger btn-xs btn-editMember" data-id="{{$m->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                    <button type="button" class="btn btn-danger btn-xs btn-viewFoto" data-cover="{{$m->foto}}" data-id="{{$m->id}}"><span class="glyphicon glyphicon-glyphicon glyphicon-picture"></span></button>
                    <button type="button" class="btn btn-danger btn-xs btn-delMember" data-id="{{$m->id}}"><span class="glyphicon glyphicon-trash"></span> </button>
                  </div>
              </td>
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
@stop

<!-- /modal -->
@section('modal-title')
Tambah Buku
@stop
@section('modal-body')
  <!-- form-group -->
  {{Form::open(array('url' => 'tambahMember', 'files' =>true ))}}
  <div class="form-group">
    <label>Foto</label>
    <input type="file" name="cover" class="form-control" placeholder="cover">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Kode</label>
    <input type="text" name="kode_member" class="form-control" placeholder="Kode">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Nama</label>
    <input type="text" required name="nama" class="form-control" placeholder="Nama">
  </div>
 
  <!-- form-group -->
  <div class="form-group">
    <label>Telp</label>
    <input type="text" name="telp" class="form-control" placeholder="Telp">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Alamat</label>
    <textarea type="text" name="alamat" class="form-control" placeholder="Alamat" rows=9></textarea> 
  </div>
    
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-danger  pull-right" >Save</button>
  {{Form::close()}}
@stop
@section('modal-footer')

@stop
