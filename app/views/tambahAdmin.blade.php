@section('title')
Tambah Admin | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">

<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <h1>Tambah Admin</h1>
      {{Form::open(array('url' => 'tambahAdmin' ,'files' => true ))}}
      
      <div class="form-group">
        <label>username</label>
        <input type="text" name="username" value="" required class="form-control" placeholder="kategori">
      </div>
      <div class="form-group">
        <label>Foto</label>
        <input type="file" name="foto"  class="form-control" placeholder="kategori">
      </div>
      <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="" required class="form-control" placeholder="kategori">
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" value="" class="form-control" placeholder="email">
      </div>
     
      <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" value="" class="form-control" placeholder="password">
      </div>
      <button type="submit" class="btn btn-danger  pull-right" >Save</button>
      {{Form::close()}}
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
<script type="text/javascript">
    $("textarea").wysihtml5();
</script>
@stop

<!-- /modal -->
@section('modal-title')

@stop
@section('modal-body')
  <!-- form-group -->
  
@stop
@section('modal-footer')

@stop
