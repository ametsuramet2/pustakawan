@section('title')
Profil Perpustakaan | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">

<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <h1>Data Perpustakaan</h1>
      {{Form::open(array('url' => 'updatePerpus' ,'files' => true ))}}
      {{Form::hidden('id',$data->id)}}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Logo</label>
            <input type="file" name="cover"  class="form-control" placeholder="kategori">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Foto</label>
            <input type="file" name="foto"  class="form-control" placeholder="kategori">
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Nama Perpustakaan</label>
        <input type="text" name="nama" value="{{$data->nama}}" required class="form-control" placeholder="kategori">
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Telp</label>
            <input type="text" name="telp" value="{{$data->telp}}" class="form-control" placeholder="telp">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" value="{{$data->email}}" class="form-control" placeholder="email">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Website</label>
            <input type="url" name="website" value="{{$data->website}}" class="form-control" placeholder="website">
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Alamat Perpustakaan</label>
        <textarea name="alamat"  class="form-control" placeholder="alamat" rows=9>{{$data->alamat}}</textarea>
      </div>
      <div class="form-group">
        <label>Keterangan Perpustakaan</label>
        <textarea name="deskripsi"  class="form-control" placeholder="keterangan" rows=9>{{$data->deskripsi}}</textarea>
      </div>
      
      <button type="submit" class="btn btn-danger  pull-right" >Save</button>
      {{Form::close()}}
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
<script type="text/javascript">
    $("textarea").wysihtml5();
</script>
@stop

<!-- /modal -->
@section('modal-title')

@stop
@section('modal-body')
  <!-- form-group -->
  
@stop
@section('modal-footer')

@stop
