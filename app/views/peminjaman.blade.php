@section('title')
Data Pinjaman Buku | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">
<div class="row">
    <div class="col-md-3 pull-right">
    <button type="button" class="btn btn-danger btn-tambahPeminjaman btn-block" data-toggle="modal" data-target="#myModal">
   <span class="glyphicon glyphicon-plus"></span>   Tambah Peminjaman
    </button>


    </div>
</div>
<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th width=7%>No</th>
              <th>Judul</th>
              <th>Nama</th>
              <th>Jumlah</th>
              <th>Tgl. Pinjam</th>
              <th>Tgl. Kembali</th>
              <th>Status</th>
              <th width=10%> Aksi</th>
              
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$p)
         
            <tr data-id="{{$p->id}}">
              <td>{{$i+1}}</td>
              <td>{{$p->buku->judul}}</td>
              <td>{{$p->anggota->nama}}</td>
              <td>{{$p->jumlah}}</td>
              <td>{{$p->pinjam}}</td>
              <td>{{$p->kembali}}</td>  
              <td>
              @if($p->status)
              <span class="glyphicon glyphicon-ok"></span> Sudah dikembalikan 
              <br><small> Pada : {{date("d/m/Y", strtotime($p->updated_at))}} jam : {{date("G:i", strtotime($p->updated_at))}}</small>
              @else
              <span class="glyphicon glyphicon-remove"></span> Belum Dikembalikan
              @endif
              </td>
             
              <td>
                  <div class="btn-group" role="group" aria-label="...">
                    @if(!$p->status)
                    <button type="button" class="btn btn-danger btn-xs btn-editPeminjaman" data-id="{{$p->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                    @endif
                    @if($p->status)
                    <button type="button" class="btn btn-danger btn-xs btn-pengembalianRestore" data-id="{{$p->id}}"><span class="glyphicon glyphicon-retweet"></span></button>
                    @else
                    <button type="button" class="btn btn-danger btn-xs btn-pengembalian" data-id="{{$p->id}}"><span class="glyphicon glyphicon-ok"></span></button>
                    @endif
                    <button type="button" class="btn btn-danger btn-xs btn-delPeminjaman" data-id="{{$p->id}}"><span class="glyphicon glyphicon-trash"></span> </button>
                  </div>
              </td>
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
<script type="text/javascript">
  $(function() {
     
         $('#datetimepicker,#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            pick12HourFormat: false   
          });
      
    });
</script>
@stop

<!-- /modal -->
@section('modal-title')
Tambah Peminjaman
@stop
@section('modal-body')
  <!-- form-group -->
  {{Form::open(array('url' => 'tambahPeminjaman' ))}}
  <div class="form-group">
    <label>Judul Buku</label>
    <input type="text" name="judul_buku" required class="form-control" placeholder="Judul Buku">
    {{Form::hidden('id_buku')}}
  </div>
  
  <!-- form-group -->
  <div class="form-group">
    <label>Nama Peminjam</label>
    <input type="text" name="nama_peminjam" required class="form-control" placeholder="Nama Peminjam">
    {{Form::hidden('id_member')}}
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Jumlah</label>
    <input type="text" name="jumlah" required class="form-control" value="1">
   
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Tgl. Pinjam</label>
    <input type="text" name="pinjam" id="datetimepicker" required class="form-control" >
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Tgl. Kembali</label>
    <input type="text" name="kembali" id="datetimepicker2" required class="form-control" >
  </div>
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-danger  pull-right" >Save</button>
  {{Form::close()}}
@stop
@section('modal-footer')

@stop
