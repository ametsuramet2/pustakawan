@section('title')
Dashboard Pustakawan | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
	
		@include('menu')
	
@stop
@section('content')
	<div class="container wrapper">
		<h1>{{$perpus->nama}}</h1>
		<div class="row">
			@if($perpus->foto)
			<div class="col-md-3">
				<img src="images/{{$perpus->foto}}" width="100%">
			</div>
			<div class="col-md-9">
			@else
			<div class="col-md-12">
			@endif
			
				<div class="well dashboard">

					<h3>{{$perpus->nama}}</h3>
					<div class="row">
						<div class="col-md-9">
						{{($perpus->telp)? "<p><span class='glyphicon glyphicon-phone-alt'></span> " . $perpus->telp . "</p>" : null}}
						{{($perpus->email)? "<p><span class='glyphicon glyphicon-envelope'></span> " . $perpus->email . "</p>" : null}}
						{{($perpus->website)? "<p><span class='glyphicon glyphicon-globe'></span> " . $perpus->website . "</p>" : null}}
						{{($perpus->alamat)? "<p><span class='glyphicon glyphicon-home'></span> " . $perpus->alamat . "</p>" : null}}
						
						</div>
						<div class="col-md-3">
						{{($perpus->logo)? "<img width=100% src='images/" . $perpus->logo . "'>" : null}}
						</div>
					</div>
					{{($perpus->deskripsi)? "<h4> Deskripsi </h4> " . $perpus->deskripsi : null}}
				</div>
			</div>
		</div>

	</div> <!-- /container -->
@stop
@section('script')
@stop
