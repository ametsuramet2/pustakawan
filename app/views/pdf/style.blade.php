<style type="text/css">
   	
	body { 
		text-align: justify;
		font-family: arial ,"sans-serif"; 
	}
	h1 { 
		-weasy-bookmark-level: none;
		margin-top: 0 ;
		text-align: center;
	}
	table{
		width:100%;
		border-collapse: collapse;
	}
	th,td{
		padding:5px 10px;
		text-align: left;
	}
	thead{
		background: #dedede;
		font-size: 1.2em;
	}
	td{
		border-bottom: 1px solid #777;
	}
	.alignright{
		text-align: right;
	}
   </style>