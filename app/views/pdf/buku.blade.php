<!DOCTYPE html>
<html lang="en">
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
    <title></title>

   @include('pdf.style')
  </head>
<body>
<h1>Katalog Buku {{$perpus->nama}}</h1>

         
        <table >
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Judul</th>
              <th>Kategori</th>
             
              <th>Penulis</th>
              <th>Penerbit</th>
              <th>Tahun</th>
              <th>Jumlah</th>
             
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$b)
         
            <tr data-id="{{$b->id}}">
              <td>{{$i+$skip+1}}</td>
              <td>{{$b->code}}</td>
              <td>{{$b->judul}}</td>
              <td>{{$b->katbuku->kategori}}</td>
              
              <td>{{$b->penulis}}</td>
              <td>{{$b->penerbit}}</td>
              <td class="alignright">{{$b->tahun}}</td>
              <td class="alignright">{{$b->jumlah}}</td>
            
              
            </tr>
          @endforeach
          </tbody>
        </table>
</body>
</html>