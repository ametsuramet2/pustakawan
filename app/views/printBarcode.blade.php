@section('content')
@foreach($data as $i=>$b)
<div class="barcodeBlock">
  
          {{--*/ $barcode = date('ymd',strtotime($b->created_at)).'-'.sprintf('%03d', $b->id) /*--}}
<h5 class="truncate">{{$b->judul}}</h5>
<img src="{{asset(DNS1D::getBarcodePNGPath($barcode, 'C39'))}}" class="barcode" width="220" height="40"/>
<br>{{$barcode}}
</div>
@endforeach
@stop
@section('style')
<style type="text/css">
  
  .barcodeBlock{
     float: left;
  padding: 5px;
  border: 1px dotted black;
  text-align: center;
  font-weight: bold;
  max-width: 240px;
  }
  .truncate {
  width: 220px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
@stop
