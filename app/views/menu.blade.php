<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL::to('/')}}">Pustakawan</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        
        <li class="dropdown">
          <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Buku <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{URL::to('/buku')}}">Daftar Buku</a></li>
            <li><a href="{{URL::to('/kategori')}}">Kategori Buku</a></li>            
            <li><a href="{{URL::to('/printBarcode')}}">Print Barcode</a></li>            
            <li><a href="{{URL::to('/pdfBuku')}}">Print PDF</a></li>            
          </ul>
        </li>
        <li class="dropdown">
          <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Aktifitas <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{URL::to('/member')}}">Member</a></li>
            <li><a href="{{URL::to('/peminjaman')}}">Peminjaman</a></li>
            <li><a href="{{URL::to('/kembali')}}">Pengembalian</a></li>            
            <li><a href="{{URL::to('/visitor')}}">Visitor</a></li>            
            <li><a href="{{URL::to('/grafik')}}">Grafik</a></li>            
          </ul>
        </li>
        
      </ul>
      {{Form::open(array('url' => 'tambahVisitor' ,"class"=>"navbar-form navbar-left" ))}}
        <div class="form-group">
          <input type="text" required class="form-control search-form visitor"  name="nama" placeholder="Add Visitor">
        </div>
        {{Form::hidden('id_visitor')}}
        <button type="submit" class="btn btn-danger">add Visitor</button>
      {{Form::close()}}
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{URL::to('/')}}">Notification</a></li>
        <li class="dropdown">
          <a href="{{URL::to('/')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{Auth::user()->nama}} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{URL::to('/perpustakaan')}}">Profil Perpustakaan</a></li>
            <li><a href="{{URL::to('/admin')}}">Profil Admin</a></li>
            @if(Auth::user()->id == 1)
            <li><a href="{{URL::to('/tambahAdmin')}}">Tambah Admin</a></li>
            @endif
            <li class="divider"></li>
            <li><a href="{{URL::to('logout/')}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>