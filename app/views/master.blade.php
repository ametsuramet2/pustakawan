<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    
    {{HTML::style(asset('css/morris.css'))}}
    {{HTML::style(asset('css/bootstrap.min.css'))}}
    {{HTML::style(asset('css/bootstrap-theme.min.css'))}}
    {{HTML::style(asset('css/jquery-ui.min.css'))}}
    {{HTML::style(asset('css/jquery.dataTables.min.css'))}}
    {{HTML::style(asset('css/bootstrap-datetimepicker.min.css'))}}
    {{HTML::style(asset('css/textarea.css'))}}
    {{HTML::style(asset('css/style.css'))}}
    
    @yield('style')
   


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    @yield('menu')
    
    @yield('content')

    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">@yield('modal-title')</h4>
          </div>
          <div class="modal-body">
            @yield('modal-body')
          </div>
          <div class="modal-footer">
            @yield('modal-footer')
          </div>
        </div>
      </div>
    </div>



    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">@yield('modal-title2')</h4>
          </div>
          <div class="modal-body">
            @yield('modal-body2')
          </div>
          <div class="modal-footer">
            @yield('modal-footer2')
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{HTML::script(asset('js/jquery.min.js'))}}
    {{HTML::script(asset('js/raphael-min.js'))}}
    {{HTML::script(asset('js/morris.min.js'))}}
    {{HTML::script(asset('js/bootstrap.min.js'))}}
    {{HTML::script(asset('js/momen.js'))}}
    {{HTML::script(asset('js/bootstrap-datetimepicker.min.js'))}}
    {{HTML::script(asset('js/jquery-ui.min.js'))}}
    {{HTML::script(asset('js/jquery.dataTables.min.js'))}}
    {{HTML::script(asset('js/textarea.js'))}}
    
    {{HTML::script(asset('js/script.js'))}}
   	@yield('script')
    <script type="text/javascript">var base="{{URL::to('/')}}"</script>

  </body>
</html>