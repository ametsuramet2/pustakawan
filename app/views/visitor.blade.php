@section('title')
Kunjungan Perpustakaan | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">

<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th width=7%>No</th>
              <th width=10%>Tgl</th>
              <th>Nama Pengunjung</th>
              <th>Status</th>
              
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$v)
         
            <tr data-id="{{$v->id}}">
              <td>{{$i+1}}</td>
              <td>
              <span class="glyphicon glyphicon-calendar"></span> {{date('d/m/y', strtotime($v->created_at))}}<br>
              <span class="glyphicon glyphicon-time"></span> {{date('G:i', strtotime($v->created_at))}}
              </td>
              <td>{{$v->nama}}</td>
              <td>
              @if($v->id_visitor)
               <span class="glyphicon glyphicon-user"></span>  Member
              @else
                  Pengunjung
              @endif
              </td>
             
              
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
@stop

<!-- /modal -->
@section('modal-title')
Tambah Kategori
@stop
@section('modal-body')
  <!-- form-group -->
  {{Form::open(array('url' => 'tambahKat' ))}}
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" name="kategori" required class="form-control" placeholder="kategori">
  </div>
  
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-danger  pull-right" >Save</button>
  {{Form::close()}}
@stop
@section('modal-footer')

@stop
