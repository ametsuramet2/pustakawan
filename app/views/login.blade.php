@section('title')
Login For Pustakawan | SuprbSOFT Technology 2015
@stop
@section('style')
{{HTML::style(asset('css/login.css'))}}
@stop
@section('menu')
@stop
@section('content')
 <div class="container">

      {{ Form::open(array('url' => 'login','class'=>'form-signin')) }}
       @if ($error = $errors->first('password'))
         <div class="alert alert-danger alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {{ $error }}
        </div>
      @endif
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="Email address or user name" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember_me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      {{Form::close()}}


    </div> <!-- /container -->
@stop
@section('script')
@stop
