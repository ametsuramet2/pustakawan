@section('title')
Grafik Aktifitas | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">
<div class="row">
    <div class="col-md-3 pull-right">
   

    </div>
</div>
<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
  <h1>Grafik Aktifitas {{$perpus->nama}}</h1>
      <div id="chart1"></div>
  </div>
  <div class="col-md-12 paddingtop20">
    
       <div class="text-center">
       <div class="btn-group" role="group" >
        <button type="button" class="btn btn-danger  btn-editBuku" onclick="location.href='grafik?range=lastweek'">Last Week</button>
        <button type="button" class="btn btn-danger  btn-editBuku" onclick="location.href='grafik'">This Week</button>
        <button type="button" class="btn btn-danger  btn-viewCover" onclick="location.href='grafik?range=thismonth'">This Month</button>
        <button type="button" class="btn btn-danger  btn-delBuku" onclick="location.href='grafik?range=lastmonth'">Last Month</button>
        <button type="button" class="btn btn-danger  btn-delBuku" onclick="location.href='grafik?range=thisyear'">This Year</button>
      </div>
     
    </div>
  </div>
</div>

{{--*/ $time = 'thisweek' /*--}} 
@if(isset($_GET['range']))
  {{--*/ $time = $_GET['range'] /*--}} 
@endif
{{--*/ $range = Grafik::Peminjaman($time) /*--}} 
{{--*/ $range2 = Grafik::Pengembalian($time) /*--}} 
{{--*/ $range3 = Grafik::Pengunjung($time) /*--}} 
{{--*/ $numItems = count($range);
$i = 0;/*--}} 

</div> <!-- /container -->
@stop
@section('script')
<script type="text/javascript">
  var data = [
  @foreach($range as $index=>$rec)
    {y: "{{$rec['tgl']}}", item1: {{$rec['jumlah']}}, item2: {{$range2[$index]['jumlah']}}, item3: {{$range3[$index]['jumlah']}}}{{(++$i !== $numItems)?',':null}}
  @endforeach
  ];
//console.log(data_week);
  new Morris.Line({    
    element: 'chart1',    
    data: data,   
    xkey: 'y',    
    ykeys: ['item1','item2','item3'],   
    labels: ['Peminjaman','Pengembalian','Pengunjung'],
    lineColors: ['#5cb85c','#5bc0de','#d9534f'],
    hideHover: 'auto',
  });
</script>
@stop

<!-- /modal -->
@section('modal-title')

@stop
@section('modal-body')

@stop
@section('modal-footer')

@stop
