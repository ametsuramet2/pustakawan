@section('title')
Kategori Buku | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">
<div class="row">
    <div class="col-md-3 pull-right">
    <button type="button" class="btn btn-danger btn-tambahKat btn-block" data-toggle="modal" data-target="#myModal">
   <span class="glyphicon glyphicon-plus"></span>   Tambah Kategori
    </button>


    </div>
</div>
<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th width=7%>No</th>
              <th>Kategori</th>
              <th width=10%> Aksi</th>
              
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$k)
         
            <tr data-id="{{$k->id}}">
              <td>{{$i+1}}</td>
              <td>{{$k->kategori}}</td>
             
              <td>
                  <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-danger btn-xs btn-editKat" data-id="{{$k->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                    
                    <button type="button" class="btn btn-danger btn-xs tn-delKat" data-id="{{$k->id}}"><span class="glyphicon glyphicon-trash"></span> </button>
                  </div>
              </td>
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
@stop

<!-- /modal -->
@section('modal-title')
Tambah Kategori
@stop
@section('modal-body')
  <!-- form-group -->
  {{Form::open(array('url' => 'tambahKat' ))}}
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" name="kategori" required class="form-control" placeholder="kategori">
  </div>
  
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-danger  pull-right" >Save</button>
  {{Form::close()}}
@stop
@section('modal-footer')

@stop
