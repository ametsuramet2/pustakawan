@section('title')
Data Pinjaman Buku | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">

<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th width=15%>Tgl</th>
              <th>Judul</th>
              <th>Nama</th>
            
              <th>Tgl. Pinjam</th>
              <th>Tgl. Pengembalian</th>
             
              
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$p)
         
            <tr data-id="{{$p->id}}">
              <td>{{date("d/m/Y G:i", strtotime($p->updated_at))}}</td>
              <td>{{$p->buku->judul}}</td>
              <td>{{$p->anggota->nama}}</td>
            
              <td>{{$p->pinjam}}</td>
              <td>{{$p->kembali}}</td>  
             
             
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
<script type="text/javascript">
  $(function() {
     
         $('#datetimepicker,#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            pick12HourFormat: false   
          });
      
    });
</script>
@stop
