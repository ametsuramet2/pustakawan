@section('title')
Daftar Buku | SuprbSOFT Technology 2015
@stop
@section('style')

@stop
@section('menu')
  @include('menu')
@stop
@section('content')
<div class="container wrapper">
<div class="row">
    <div class="col-md-3 pull-right">
    <button type="button" class="btn btn-danger btn-tambahBuku btn-block" data-toggle="modal" data-target="#myModal">
   <span class="glyphicon glyphicon-plus"></span>   Tambah Buku
    </button>


    </div>
</div>
<div class="row paddingtop20">
  @include('alert')
  <div class="col-md-12">
      <div class="table-responsive">          
        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Barcode</th>
              <th>Judul</th>
              <th>Kategori</th>
              <th>Status</th>
              <th>Penulis</th>
              <th>Penerbit</th>
              <th>Tahun</th>
              <th>Jumlah</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          @foreach($data as $i=>$b)
          {{--*/ $barcode = date('ymd',strtotime($b->created_at)).'-'.sprintf('%03d', $b->id) /*--}}
         
            <tr data-id="{{$b->id}}">
              <td>{{$i+1}}</td>
              <td>{{$b->code}}</td>
              <td style="text-align:center;font-weight:bold"><img src="{{asset(DNS1D::getBarcodePNGPath($barcode, 'C39'))}}" class="barcode" width="240" height="40"/>
              <br>{{$barcode}}
              </td>
              <td>{{$b->judul}}</td>
              <td>{{$b->katbuku->kategori}}</td>
              <td>
                @if($b->status==2)
                    Titip : <br> {{$b->pemilik}}
                @else
                    Donasi
                @endif
              </td>
              <td>{{$b->penulis}}</td>
              <td>{{$b->penerbit}}</td>
              <td>{{$b->tahun}}</td>
              <td>{{$b->jumlah}}</td>
              <td>
                  <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-danger btn-xs btn-editBuku" data-id="{{$b->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                    <button type="button" class="btn btn-danger btn-xs btn-viewCover" data-cover="{{$b->cover}}" data-id="{{$b->id}}"><span class="glyphicon glyphicon-glyphicon glyphicon-picture"></span></button>
                    <button type="button" class="btn btn-danger btn-xs btn-delBuku" data-id="{{$b->id}}"><span class="glyphicon glyphicon-trash"></span> </button>
                  </div>
              </td>
              
            </tr>
          @endforeach
          </tbody>
        </table>
        </div>
  </div>
</div>
  

</div> <!-- /container -->
@stop
@section('script')
@stop

<!-- /modal -->
@section('modal-title')
Tambah Buku
@stop
@section('modal-body')
  <!-- form-group -->
  {{Form::open(array('url' => 'tambahBuku', 'files' =>true ))}}
  <div class="form-group">
    <label>Cover</label>
    <input type="file" name="cover" class="form-control" placeholder="cover">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Kode</label>
    <input type="text" name="kode" class="form-control" placeholder="Kode">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Judul</label>
    <input type="text" required name="judul" class="form-control" placeholder="Judul">
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select  name="kategori" class="form-control">
      @foreach($kategori as $kat)
        <option value="{{$kat->id}}">{{$kat->kategori}}</option>
      @endforeach
    </select>
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Penulis</label>
    <input type="text" name="penulis" class="form-control" placeholder="Penulis">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Penerbit</label>
    <input type="text" name="penerbit" class="form-control" placeholder="Penerbit">
  </div>
  <!-- form-group -->
  <div class="form-group">
    <label>Tahun</label>
    <input type="number" name="tahun" class="form-control" placeholder="Tahun">
  </div>
  <div class="form-group">
    <label>Jumlah</label>
    <input type="number" name="jumlah" class="form-control" placeholder="Jumlah">
  </div>
  <div class="form-group">
    <label>Pemilik</label>
    <input type="text" name="pemilik" class="form-control" placeholder="Pemilik">
  </div>
  <div class="form-group">
    <label>Status Buku</label>
    <select  name="status" class="form-control">
     
        <option value="1">Donasi</option>
        <option value="2">Titip</option>
     
    </select>
  </div>
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-danger  pull-right" >Save</button>
  {{Form::close()}}
@stop
@section('modal-footer')

@stop
