$(document).ready(function(){
	$('.table').DataTable({
		"aLengthMenu": [[10, 25, 50, 100, 200, 500, -1], [10, 25, 50, 100, 200, 500,"All"]],
		"iDisplayLength": 25,
	});
	$('[name=DataTables_Table_0_length]').addClass('pretty_form');
	$('input[aria-controls=DataTables_Table_0]').addClass('pretty_form');
	$('[name=kode]').change(function(){
		var $this = $(this);
		$.post('cekKode',{kode:this.value},function(data){
			if(parseInt(data)==1){
				$this.parent().append("<small class='alert alert-modal'>Kode sudah terpakai</small");
				$this.focus();
			}else{
				$('.alert-modal').remove()
			}
		});
	});
	$('[name=kode_member]').change(function(){
		var $this = $(this);
		$.post('cekKodeMember',{kode:this.value},function(data){
			if(parseInt(data)==1){
				$this.parent().append("<small class='alert alert-modal'>Kode sudah terpakai</small");
				$this.focus();
			}else{
				$('.alert-modal').remove()
			}
		});
	});
	$('.table').on('click', '.btn-editBuku', function(e){
	    var id = $(this).data('id');
		
		$.post('editBuku',{id:id},function(data){
			// console.log(data)
			var body = $('#myModal').find('.modal-body');
			body.find('form').attr('action',base+'/editBuku/'+id);
			body.find('[name=kode]').val(data.code);
			body.find('[name=judul]').val(data.judul);
			body.find('[name=kategori]').val(data.kategori);
			body.find('[name=penulis]').val(data.penulis);
			body.find('[name=penerbit]').val(data.penerbit);
			body.find('[name=tahun]').val(data.tahun);
			body.find('[name=jumlah]').val(data.jumlah);
			body.find('[name=status]').val(data.status);
			body.find('[name=pemilik]').val(data.pemilik);

			$('#myModal').modal('show');
		});
	
	});
	$('.table').on('click', '.btn-delBuku', function(e){
	
		var id = $(this).data('id');
		con = confirm('Anda yakin menghapus item tersebut?')
		if(con){
			location.href = base+'/delBuku/'+id;
		}	
		return false
	});
	$('.table').on('click', '.btn-viewCover', function(e){
	
		var parent = $(this).parents('tr');
		var title = parent.find('td:nth-child(3)').html()
		var thumb = "<h3>No Images</h3>";
		var cover = $(this).data('cover');
		$('#myModal2').find('.modal-title').html(title);
		if(cover){
			var thumb = "<div class='row'><img width='100%' src='"+base+"/images/"+cover+"' /></div>"
		}
		$('#myModal2').find('.modal-body').html(thumb);
		$('#myModal2').modal('show');
	});


	$('.table').on('click', '.btn-editKat', function(e){
	
		var id = $(this).data('id');
		$.post('editKat',{id:id},function(data){
			//console.log(data)
			var body = $('#myModal').find('.modal-body');
			body.find('form').attr('action',base+'/editKat/'+id);
			
			body.find('[name=kategori]').val(data.kategori);
			

			$('#myModal').modal('show');
		});
	});

	
	$('.table').on('click', '.btn-delKat', function(e){
	
		var id = $(this).data('id');
		con = confirm('Anda yakin menghapus item tersebut?')
		if(con){
			location.href = base+'/delKat/'+id;
		}	
		return false
	});

	$('.table').on('click', '.btn-editMember', function(e){
	
	
		var id = $(this).data('id');
		$.post('editMember',{id:id},function(data){
			console.log(data)
			var body = $('#myModal').find('.modal-body');
			body.find('form').attr('action',base+'/editMember/'+id);
			
			body.find('[name=kode_member]').val(data.kode);
			body.find('[name=nama]').val(data.nama);
			body.find('[name=telp]').val(data.telp);
			body.find('[name=alamat]').val(data.alamat);
			cleanEditor();
			$("textarea").wysihtml5();
			$('#myModal').modal('show');
		});
	});

	$('#myModal').on('show.bs.modal', function (e) {
	  		cleanEditor();
			$("textarea").wysihtml5();
	})



	
	$('.table').on('click', '.btn-viewFoto', function(e){
	
		var parent = $(this).parents('tr');
		var title = parent.find('td:nth-child(3)').html()
		var thumb = "<h3>No Images</h3>";
		var cover = $(this).data('cover');
		$('#myModal2').find('.modal-title').html(title);
		if(cover){
			var thumb = "<div class='row'><img width='100%' src='"+base+"/images/"+cover+"' /></div>"
		}
		$('#myModal2').find('.modal-body').html(thumb);
		$('#myModal2').modal('show');
	});

	$('.table').on('click', '.btn-delMember', function(e){
	
		var id = $(this).data('id');
		con = confirm('Anda yakin menghapus Member tersebut?')
		if(con){
			location.href = base+'/delMember/'+id;
		}	
		return false
	});

	

	$('[name="judul_buku"]').autocomplete({
      source: base+"/cekBuku/",
      minLength: 2,
      select: function( event, ui ) {
         	//console.log(ui.item.value)
         	this.value = ui.item.label
          //alert(ui.item.value)
          $('[name="id_buku"]').val(ui.item.value)
         
          return false;
        }
    });  
	$('[name="nama_peminjam"]').autocomplete({
      source: base+"/cekMember/",
      minLength: 2,
      select: function( event, ui ) {
         	//console.log(ui.item.value)
         	this.value = ui.item.label
          //alert(ui.item.value)
          $('[name="id_member"]').val(ui.item.value)
         
          return false;
        }
    }); 

	$('[name="pemilik"],.visitor').autocomplete({
      source: base+"/cekMember/",
      minLength: 2,
      select: function( event, ui ) {
         	//console.log(ui.item.value)
         	this.value = ui.item.label
          //alert(ui.item.value)
	        if($('[name="id_visitor"]').length)
	        $('[name="id_visitor"]').val(ui.item.value)
	        return false;
        }
    }); 

    $('.table').on('click', '.btn-editPeminjaman', function(e){

		var id = $(this).data('id');
		$.post('editPeminjaman',{id:id},function(data){
			console.log(data)
			var body = $('#myModal').find('.modal-body');
			body.find('form').attr('action',base+'/editPeminjaman/'+id);
			
			body.find('[name=judul_buku]').val(data.buku.judul);
			body.find('[name=id_buku]').val(data.id_buku);
			body.find('[name=nama_peminjam]').val(data.anggota.nama);
			body.find('[name=id_member]').val(data.id_member);
			body.find('[name=jumlah]').val(data.jumlah);
			body.find('[name=pinjam]').val(data.pinjam);
			body.find('[name=kembali]').val(data.kembali);
			
			cleanEditor();
			$("textarea").wysihtml5();
			$('#myModal').modal('show');
		});		
	});
   $('.table').on('click', '.btn-pengembalian', function(e){
 
		var id = $(this).data('id');
		location.href = base+'/pengembalian/'+id
	});

	 $('.table').on('click', '.btn-pengembalianRestore', function(e){
  
		var id = $(this).data('id');
		location.href = base+'/pengembalianRestore/'+id
	});

	 $('.table').on('click', '.btn-delPeminjaman', function(e){
  
		var id = $(this).data('id');
		con = confirm('Anda yakin menghapus item tersebut?')
		if(con){
			location.href = base+'/delPeminjaman/'+id;
		}	
		return false
	});
	 $('.btn-tambahBuku').click(function(){
	 	$('form').attr('action',base+'/tambahBuku')
	 	cleanForm();
	 });
	 $('.btn-tambahMember').click(function(){
	 	$('form').attr('action',base+'/tambahMember')
	 	cleanForm();
	 });
	 $('.btn-tambahKat').click(function(){
	 	$('form').attr('action',base+'/tambahKat')
	 	cleanForm();
	 });
	 $('.btn-tambahPeminjaman').click(function(){
	 	$('form').attr('action',base+'/tambahPeminjaman')
	 	cleanForm();
	 });
});

function cleanEditor(){
	$("textarea").show();
	$(".wysihtml5-toolbar").remove();
	$("iframe.wysihtml5-sandbox, input[name='_wysihtml5_mode']").remove();
	$("body").removeClass("wysihtml5-supported");
}

function cleanForm(){
	$("input").val(null);
	$("textarea").val(null).html(null);
	$("select").val(null);
	
}